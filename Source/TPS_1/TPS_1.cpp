// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPS_1.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TPS_1, "TPS_1" );

DEFINE_LOG_CATEGORY(LogTPS_1)
 