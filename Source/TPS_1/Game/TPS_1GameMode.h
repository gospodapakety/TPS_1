// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TPS_1GameMode.generated.h"

UCLASS(minimalapi)
class ATPS_1GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATPS_1GameMode();
};



