// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TPS_1/FuncLibrary/Types.h"
#include "Engine/DataTable.h"
#include "TPS_1/WeaponDefault.h"
#include "TPS_1GameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TPS_1_API UTPS_1GameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	//table
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = " WeaponSetting ")
		UDataTable* WeaponInfoTable = nullptr;
	UFUNCTION(BlueprintCallable)
		bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);
};
