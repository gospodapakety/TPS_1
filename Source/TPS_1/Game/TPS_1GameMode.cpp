// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPS_1GameMode.h"
#include "TPS_1PlayerController.h"
#include "TPS_1/Character/TPS_1Character.h"
#include "UObject/ConstructorHelpers.h"

ATPS_1GameMode::ATPS_1GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATPS_1PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}