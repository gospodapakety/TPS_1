// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS_1/Game/TPS_1GameInstance.h"

bool UTPS_1GameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
	bool bIsFind = false;
	FWeaponInfo* WeaponInfoRow;


	if(WeaponInfoTable)
	{
		WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);
		if (WeaponInfoRow)
		{
			bIsFind = true;
			OutInfo = *WeaponInfoRow;
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("UTPS_1GameInstance::GetWeaponInfoByName - WeaponTable - NULL"));
		}
	}

	return bIsFind;
}

